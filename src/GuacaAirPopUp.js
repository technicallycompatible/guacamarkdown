import $ from 'jquery';

export default class GuacaAirPopUp {

	constructor(editorElement, controlHandlerService, settings) {
		this.settings = settings || this.getDefaultSettings();
		this.controlHandlerService = controlHandlerService;
		this.airPopUp = $('<div></div>');
		this.airPopUp.addClass("guaca-airpopup");
		this.airPopUp.insertAfter(editorElement);
		this.subscribeToEvents();
		this.currentControls = [];
		this.addControls(this.settings.controls, this.airPopUp);
		this.$window = $(window);
		this.hasFocus = false;
	}

	// API Start

	setPosition(rect) {
		var y = this.getComputedYPosition(rect.top, rect.bottom),
			x = this.getComputedXPosition(rect.left);

		this.updateControlStates();
		this.airPopUp.css("top", y);
		this.airPopUp.css("left", x);
		this.airPopUp.css("display", "block");
	}

	hide() {
		this.airPopUp.css("display", "none");
	}

	getHeight() {
		return this.airPopUp.innerHeight();
	}

	getWidth() {
		return this.airPopUp.innerWidth();
	}

	updateControlStates() {
		this.currentControls.forEach((control) => {
			var enabledState = false;
			if(control.handler)
				enabledState = control.handler.isEnabledOnSelection();
			if(enabledState) {
				control.elem.addClass(this.settings.controlActiveClass);
			} else {
				control.elem.removeClass(this.settings.controlActiveClass);
			}
		});
	}

	// API End

	subscribeToEvents() {
		var self = this;
		self.airPopUp.on("focusin", () => {
			self.hasFocus = true;
			self.airPopUp.one('focusout', () => {
				self.hasFocus = false;
			});
		});
	}

	getComputedYPosition(top, bottom) {
		var newYOffset = this.getHeight() + this.settings["y-spacing"];
		var y;
		if (top < newYOffset) y = bottom + this.settings["y-spacing"];
		else y = top - newYOffset;
		return y + this.$window.scrollTop();
	}

	getComputedXPosition(left) {
		var x = left;
		if (left + this.getWidth() + this.settings["x-spacing"] > this.$window.innerWidth())
			x = this.$window.innerWidth() - this.getWidth() - this.settings["x-spacing"];
		return x + this.$window.scrollLeft();
	}

	addControls(controls, element) {
		var self = this;
		for(var control of controls) {
			if(typeof control == "object" && control.length) {
				var container =  $('<span class="container"></span>');
				this.addControls(control, container);
				element.append(container);
			}
			var controlElem = this.createControl(control);
			element.append(controlElem);
			var controlHandler = this.controlHandlerService.getHandler(control);
			this.currentControls.push({ name: control, elem: controlElem, handler: controlHandler });
			if(controlHandler) {
				controlElem.on("click", function() {
					var controlAlias = $(this).attr("control-alias");
					var controlHandler = self.controlHandlerService.getHandler(controlAlias);
					controlHandler.execute();
					self.updateControlStates();
				});
			}
		}
	}

	createControl(className) {
		var control = $('<button></button>');
		control.addClass("guaca-control");
		control.addClass(className);
		control.attr("control-alias", className);
		return control;
	}

	getDefaultControls() {
		return [
			'bold',
			'italic',
			'divider',
			[
				'small-heading',
				'medium-heading',
				'large-heading'
			],
			'paragraph',
			'ul',
			'ol'
		];
	}

	getDefaultSettings() {
		return {
			"y-spacing": 8,
			"x-spacing": 20,
			"controls": this.getDefaultControls(),
			"controlActiveClass": "control-active"
		}
	}
}
