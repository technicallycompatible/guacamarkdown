import BoldMarkupHandler from './handlers/BoldMarkupHandler';
import ItalicMarkupHandler from './handlers/ItalicMarkupHandler';
import UnorderedListMarkupHandler from './handlers/UnorderedListMarkupHandler';
import OrderedListMarkupHandler from './handlers/OrderedListMarkupHandler';
import HeadingMarkupHandler from './handlers/HeadingMarkupHandler';
import FormatBlockMarkupHandler from './handlers/FormatBlockMarkupHandler';

export default [
	{
		alias: "bold",
		handlerClass: BoldMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "italic",
		handlerClass: ItalicMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "ul",
		handlerClass: UnorderedListMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "ol",
		handlerClass: OrderedListMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "large-heading",
		handlerClass: FormatBlockMarkupHandler,
		handlerProperty: "H1"
	},
	{
		alias: "medium-heading",
		handlerClass: FormatBlockMarkupHandler,
		handlerProperty: "H2"
	},
	{
		alias: "small-heading",
		handlerClass: FormatBlockMarkupHandler,
		handlerProperty: "H3"
	}
]
