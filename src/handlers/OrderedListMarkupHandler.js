import DocumentCommandHandler from './DocumentCommandHandler';

export default class OrderedListMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "insertOrderedList";
	}
}
