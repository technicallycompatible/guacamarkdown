import DocumentCommandHandler from './DocumentCommandHandler';

export default class BoldMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "bold";
	}
}
