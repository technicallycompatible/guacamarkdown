import BaseHandler from './BaseHandler';

export default class DocumentCommandHandler extends BaseHandler {

	constructor(commandProperty) {
		super();
		this.commandName = null;
		this.commandProperty = commandProperty;
	}

	execute() {
		if (document.execCommand && this.commandName) {
			document.execCommand(this.commandName, false, this.commandProperty);
		}
		else {
			throw "Browsers without document.execCommand are not supported in DocumentCommandHandler";
		}
	}

	isEnabledOnSelection() {
		if (document.queryCommandState && this.commandName) {
			var commandIsApplied = false;
			commandIsApplied = document.queryCommandState(this.commandName);
			return commandIsApplied;
		}
		else {
			throw "Browsers without document.queryCommandState are not supported in DocumentCommandHandler";
		}
	}
}
