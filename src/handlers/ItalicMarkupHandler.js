import DocumentCommandHandler from './DocumentCommandHandler';

export default class ItalicMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "italic";
	}
}
