import DocumentCommandHandler from './DocumentCommandHandler';

export default class FormatBlockMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "formatBlock";
	}
}
