import DocumentCommandHandler from './DocumentCommandHandler';

export default class UnorderedListMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "insertUnorderedList";
	}
}
