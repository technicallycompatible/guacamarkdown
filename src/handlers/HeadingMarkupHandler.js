import DocumentCommandHandler from './DocumentCommandHandler';

export default class HeadingMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "heading";
	}
}
