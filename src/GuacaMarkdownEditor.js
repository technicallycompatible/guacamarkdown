"use strict";

import GuacaAirPopUp from './GuacaAirPopUp';
import ControlHandlerService from './ControlHandlerService';
import DefaultHandlerConfig from './DefaultHandlerConfig';
import toMarkdown from 'to-markdown';
import marked from 'marked';
import $ from 'jquery';

class GuacaMarkdownEditor {

	constructor(inputElement) {
		var self = this;
		self.inputElement = inputElement;
		self.guacaEditorElement = this.setUpGuacaMarkdownElement();
		self.updateGuacaElementFromInputElement();
		self.controlHandlerService = this.setUpControlHandlerService();
		self.airPopUp = new GuacaAirPopUp(self.guacaEditorElement, self.controlHandlerService);
		self.currentSelection = null;

		inputElement.css("display", "none");
		self.subscribeToEvents();
	}

	// API Start

	getCurrentSelection() {
		return this.currentSelection;
	}

	onMarkdownUpdate() {}

	updateGuacaElement() {
		this.updateGuacaElementFromInputElement();
	}

	// API End

	setUpGuacaMarkdownElement() {
		var guacaElement = $('<div contentEditable="true" class="guaca-editor" ></div>');
		var attributes = this.inputElement.prop("attributes");

		$.each(attributes, (i, a) => {
			guacaElement.attr(a.name, a.value);
		});

		guacaElement.css("display", "inline-block");
		guacaElement.addClass("guaca-editor");
		guacaElement.insertAfter(this.inputElement);

		return guacaElement;
	}

	setUpControlHandlerService() {
		var controlHandlerService = new ControlHandlerService();
		for(var handler of DefaultHandlerConfig)
		{
			var HandlerClass = handler.handlerClass;
			var handlerInstance = new HandlerClass(handler.handlerProperty);
			handlerInstance.registerMarkdownEditor(this);
			controlHandlerService.registerHandler(handler.alias, handlerInstance);
		}

		return controlHandlerService;
	}

	subscribeToEvents() {
		var self = this;
		var selectStartEventName = "selectstart";
		var selectStartAvailable = typeof this.guacaEditorElement[0].onselectstart !== "undefined";
		if(!selectStartAvailable) selectStartEventName = "mousedown";

		this.guacaEditorElement.on(selectStartEventName, () => {
			self.onSelectStart();
			$(document).one('mouseup', () => {
				setTimeout(function(){
					self.onSelectText();
				},1);
			});
			return true;
		});

		this.guacaEditorElement.on('blur', (event) => {
			// neccessary in order to get next focus
			// no cross-browser solution available
			setTimeout(function(){
				self.onEditorBlur();
			},1);
		});

		this.guacaEditorElement.on('input', (event) => {
			var newHtml = event.target.innerHTML;
			var newMarkdown = toMarkdown(newHtml);
			self.inputElement.val(newMarkdown);
			if(self.onMarkdownUpdate)
			self.onMarkdownUpdate();
		});

		this.inputElement.on('input', (event) => {
			self.updateGuacaElementFromInputElement();
		});
	}

	updateGuacaElementFromInputElement() {
		this.guacaEditorElement[0].innerHTML = marked(this.inputElement[0].value);
	}

	onEditorBlur() {
		var currentEle = $(document.activeElement);
		if (currentEle.hasClass("guaca-control"))
			return true;
		else
			this.airPopUp.hide();
	}

	onSelectText() {
		this.currentSelection = window.getSelection();
		if(!this.currentSelection.type && this.currentSelection.anchorOffset === this.currentSelection.focusOffset)
			return;
		if(this.currentSelection.type === "Caret")
			return;
		var rect = this.currentSelection.getRangeAt(0).getBoundingClientRect();
		this.airPopUp.setPosition(rect);
	}

	onSelectStart() {
		this.airPopUp.hide();
	}
}

export default GuacaMarkdownEditor;
