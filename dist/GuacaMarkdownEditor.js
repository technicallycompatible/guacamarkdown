'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _toMarkdown = require('to-markdown');

var _toMarkdown2 = _interopRequireDefault(_toMarkdown);

var _marked = require('marked');

var _marked2 = _interopRequireDefault(_marked);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GuacaAirPopUp = function () {
	function GuacaAirPopUp(editorElement, controlHandlerService, settings) {
		_classCallCheck(this, GuacaAirPopUp);

		this.settings = settings || this.getDefaultSettings();
		this.controlHandlerService = controlHandlerService;
		this.airPopUp = (0, _jquery2.default)('<div></div>');
		this.airPopUp.addClass("guaca-airpopup");
		this.airPopUp.insertAfter(editorElement);
		this.subscribeToEvents();
		this.currentControls = [];
		this.addControls(this.settings.controls, this.airPopUp);
		this.$window = (0, _jquery2.default)(window);
		this.hasFocus = false;
	}

	// API Start

	_createClass(GuacaAirPopUp, [{
		key: 'setPosition',
		value: function setPosition(rect) {
			var y = this.getComputedYPosition(rect.top, rect.bottom),
			    x = this.getComputedXPosition(rect.left);

			this.updateControlStates();
			this.airPopUp.css("top", y);
			this.airPopUp.css("left", x);
			this.airPopUp.css("display", "block");
		}
	}, {
		key: 'hide',
		value: function hide() {
			this.airPopUp.css("display", "none");
		}
	}, {
		key: 'getHeight',
		value: function getHeight() {
			return this.airPopUp.innerHeight();
		}
	}, {
		key: 'getWidth',
		value: function getWidth() {
			return this.airPopUp.innerWidth();
		}
	}, {
		key: 'updateControlStates',
		value: function updateControlStates() {
			var _this = this;

			this.currentControls.forEach(function (control) {
				var enabledState = false;
				if (control.handler) enabledState = control.handler.isEnabledOnSelection();
				if (enabledState) {
					control.elem.addClass(_this.settings.controlActiveClass);
				} else {
					control.elem.removeClass(_this.settings.controlActiveClass);
				}
			});
		}

		// API End

	}, {
		key: 'subscribeToEvents',
		value: function subscribeToEvents() {
			var self = this;
			self.airPopUp.on("focusin", function () {
				self.hasFocus = true;
				self.airPopUp.one('focusout', function () {
					self.hasFocus = false;
				});
			});
		}
	}, {
		key: 'getComputedYPosition',
		value: function getComputedYPosition(top, bottom) {
			var newYOffset = this.getHeight() + this.settings["y-spacing"];
			var y;
			if (top < newYOffset) y = bottom + this.settings["y-spacing"];else y = top - newYOffset;
			return y + this.$window.scrollTop();
		}
	}, {
		key: 'getComputedXPosition',
		value: function getComputedXPosition(left) {
			var x = left;
			if (left + this.getWidth() + this.settings["x-spacing"] > this.$window.innerWidth()) x = this.$window.innerWidth() - this.getWidth() - this.settings["x-spacing"];
			return x + this.$window.scrollLeft();
		}
	}, {
		key: 'addControls',
		value: function addControls(controls, element) {
			var self = this;
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = controls[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var control = _step.value;

					if ((typeof control === 'undefined' ? 'undefined' : _typeof(control)) == "object" && control.length) {
						var container = (0, _jquery2.default)('<span class="container"></span>');
						this.addControls(control, container);
						element.append(container);
					}
					var controlElem = this.createControl(control);
					element.append(controlElem);
					var controlHandler = this.controlHandlerService.getHandler(control);
					this.currentControls.push({ name: control, elem: controlElem, handler: controlHandler });
					if (controlHandler) {
						controlElem.on("click", function () {
							var controlAlias = (0, _jquery2.default)(this).attr("control-alias");
							var controlHandler = self.controlHandlerService.getHandler(controlAlias);
							controlHandler.execute();
							self.updateControlStates();
						});
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
		}
	}, {
		key: 'createControl',
		value: function createControl(className) {
			var control = (0, _jquery2.default)('<button></button>');
			control.addClass("guaca-control");
			control.addClass(className);
			control.attr("control-alias", className);
			return control;
		}
	}, {
		key: 'getDefaultControls',
		value: function getDefaultControls() {
			return ['bold', 'italic', 'divider', ['small-heading', 'medium-heading', 'large-heading'], 'paragraph', 'ul', 'ol', 'code'];
		}
	}, {
		key: 'getDefaultSettings',
		value: function getDefaultSettings() {
			return {
				"y-spacing": 8,
				"x-spacing": 20,
				"controls": this.getDefaultControls(),
				"controlActiveClass": "control-active"
			};
		}
	}]);

	return GuacaAirPopUp;
}();

var BaseHandler = function () {
	function BaseHandler() {
		_classCallCheck(this, BaseHandler);

		this.markdownEditor = null;
	}

	_createClass(BaseHandler, [{
		key: 'registerMarkdownEditor',
		value: function registerMarkdownEditor(markdownEditor) {
			this.markdownEditor = markdownEditor;
		}
	}, {
		key: 'getCurrentSelection',
		value: function getCurrentSelection() {
			if (this.markdownEditor) {
				return this.markdownEditor.getCurrentSelection();
			}
			throw "registerMarkdownEditor must be called before calling getCurrentSelection.";
		}
	}, {
		key: 'execute',
		value: function execute() {
			throw "Must override the execute method in extending classes";
		}
	}, {
		key: 'isEnabledOnSelection',
		value: function isEnabledOnSelection() {
			throw "Must override the isEnabledOnSelection method in extending classes";
		}
	}]);

	return BaseHandler;
}();

var ControlHandlerService = function () {
	function ControlHandlerService() {
		_classCallCheck(this, ControlHandlerService);

		this.registeredHandlers = [];
	}

	_createClass(ControlHandlerService, [{
		key: 'registerHandler',
		value: function registerHandler(alias, handler) {
			var isControllerInstance = handler instanceof BaseHandler;
			if (!isControllerInstance) throw "ControlHandlerService.registerHandler can only register BaseHandler extensions";
			var existingHandlerReg = this.registeredHandlers.find(function (h) {
				return h.handler === handler;
			});
			if (existingHandlerReg) {
				existingHandlerReg.aliases.push(alias);
				return;
			}

			this.registeredHandlers.push({
				aliases: [alias],
				handler: handler
			});
		}
	}, {
		key: 'getHandler',
		value: function getHandler(alias) {
			var handlerRegistration = this.registeredHandlers.find(function (h) {
				return h.aliases.some(function (a) {
					return a === alias;
				});
			});
			if (handlerRegistration) return handlerRegistration.handler;else return null;
		}
	}]);

	return ControlHandlerService;
}();

var DocumentCommandHandler = function (_BaseHandler) {
	_inherits(DocumentCommandHandler, _BaseHandler);

	function DocumentCommandHandler(commandProperty) {
		_classCallCheck(this, DocumentCommandHandler);

		var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(DocumentCommandHandler).call(this));

		_this2.commandName = null;
		_this2.commandProperty = commandProperty;
		return _this2;
	}

	_createClass(DocumentCommandHandler, [{
		key: 'execute',
		value: function execute() {
			if (document.execCommand && this.commandName) {
				document.execCommand(this.commandName, false, this.commandProperty);
			} else {
				throw "Browsers without document.execCommand are not supported in DocumentCommandHandler";
			}
		}
	}, {
		key: 'isEnabledOnSelection',
		value: function isEnabledOnSelection() {
			if (document.queryCommandState && this.commandName) {
				var commandIsApplied = false;
				commandIsApplied = document.queryCommandState(this.commandName);
				return commandIsApplied;
			} else {
				throw "Browsers without document.queryCommandState are not supported in DocumentCommandHandler";
			}
		}
	}]);

	return DocumentCommandHandler;
}(BaseHandler);

var BoldMarkupHandler = function (_DocumentCommandHandl) {
	_inherits(BoldMarkupHandler, _DocumentCommandHandl);

	function BoldMarkupHandler(commandProperty) {
		_classCallCheck(this, BoldMarkupHandler);

		var _this3 = _possibleConstructorReturn(this, Object.getPrototypeOf(BoldMarkupHandler).call(this, commandProperty));

		_this3.commandName = "bold";
		return _this3;
	}

	return BoldMarkupHandler;
}(DocumentCommandHandler);

var ItalicMarkupHandler = function (_DocumentCommandHandl2) {
	_inherits(ItalicMarkupHandler, _DocumentCommandHandl2);

	function ItalicMarkupHandler(commandProperty) {
		_classCallCheck(this, ItalicMarkupHandler);

		var _this4 = _possibleConstructorReturn(this, Object.getPrototypeOf(ItalicMarkupHandler).call(this, commandProperty));

		_this4.commandName = "italic";
		return _this4;
	}

	return ItalicMarkupHandler;
}(DocumentCommandHandler);

var UnorderedListMarkupHandler = function (_DocumentCommandHandl3) {
	_inherits(UnorderedListMarkupHandler, _DocumentCommandHandl3);

	function UnorderedListMarkupHandler(commandProperty) {
		_classCallCheck(this, UnorderedListMarkupHandler);

		var _this5 = _possibleConstructorReturn(this, Object.getPrototypeOf(UnorderedListMarkupHandler).call(this, commandProperty));

		_this5.commandName = "insertUnorderedList";
		return _this5;
	}

	return UnorderedListMarkupHandler;
}(DocumentCommandHandler);

var OrderedListMarkupHandler = function (_DocumentCommandHandl4) {
	_inherits(OrderedListMarkupHandler, _DocumentCommandHandl4);

	function OrderedListMarkupHandler(commandProperty) {
		_classCallCheck(this, OrderedListMarkupHandler);

		var _this6 = _possibleConstructorReturn(this, Object.getPrototypeOf(OrderedListMarkupHandler).call(this, commandProperty));

		_this6.commandName = "insertOrderedList";
		return _this6;
	}

	return OrderedListMarkupHandler;
}(DocumentCommandHandler);

var HeadingMarkupHandler = function (_DocumentCommandHandl5) {
	_inherits(HeadingMarkupHandler, _DocumentCommandHandl5);

	function HeadingMarkupHandler(commandProperty) {
		_classCallCheck(this, HeadingMarkupHandler);

		var _this7 = _possibleConstructorReturn(this, Object.getPrototypeOf(HeadingMarkupHandler).call(this, commandProperty));

		_this7.commandName = "heading";
		return _this7;
	}

	return HeadingMarkupHandler;
}(DocumentCommandHandler);

var FormatBlockMarkupHandler = function (_DocumentCommandHandl6) {
	_inherits(FormatBlockMarkupHandler, _DocumentCommandHandl6);

	function FormatBlockMarkupHandler(commandProperty) {
		_classCallCheck(this, FormatBlockMarkupHandler);

		var _this8 = _possibleConstructorReturn(this, Object.getPrototypeOf(FormatBlockMarkupHandler).call(this, commandProperty));

		_this8.commandName = "formatBlock";
		return _this8;
	}

	return FormatBlockMarkupHandler;
}(DocumentCommandHandler);

var DefaultHandlerConfig = [{
	alias: "bold",
	handlerClass: BoldMarkupHandler,
	handlerProperty: null
}, {
	alias: "italic",
	handlerClass: ItalicMarkupHandler,
	handlerProperty: null
}, {
	alias: "ul",
	handlerClass: UnorderedListMarkupHandler,
	handlerProperty: null
}, {
	alias: "ol",
	handlerClass: OrderedListMarkupHandler,
	handlerProperty: null
}, {
	alias: "large-heading",
	handlerClass: HeadingMarkupHandler,
	handlerProperty: "H1"
}, {
	alias: "medium-heading",
	handlerClass: HeadingMarkupHandler,
	handlerProperty: "H2"
}, {
	alias: "small-heading",
	handlerClass: HeadingMarkupHandler,
	handlerProperty: "H3"
}, {
	alias: "code",
	handlerClass: FormatBlockMarkupHandler,
	handlerProperty: "code"
}];

var GuacaMarkdownEditor = function () {
	function GuacaMarkdownEditor(inputElement) {
		_classCallCheck(this, GuacaMarkdownEditor);

		var self = this;
		self.inputElement = inputElement;
		self.guacaEditorElement = this.setUpGuacaMarkdownElement();
		self.updateGuacaElementFromInputElement();
		self.controlHandlerService = this.setUpControlHandlerService();
		self.airPopUp = new GuacaAirPopUp(self.guacaEditorElement, self.controlHandlerService);
		self.currentSelection = null;

		inputElement.css("display", "none");
		self.subscribeToEvents();
	}

	// API Start

	_createClass(GuacaMarkdownEditor, [{
		key: 'getCurrentSelection',
		value: function getCurrentSelection() {
			return this.currentSelection;
		}
	}, {
		key: 'onMarkdownUpdate',
		value: function onMarkdownUpdate() {}
	}, {
		key: 'updateGuacaElement',
		value: function updateGuacaElement() {
			this.updateGuacaElementFromInputElement();
		}

		// API End

	}, {
		key: 'setUpGuacaMarkdownElement',
		value: function setUpGuacaMarkdownElement() {
			var guacaElement = (0, _jquery2.default)('<div contentEditable="true" class="guaca-editor" ></div>');
			var attributes = this.inputElement.prop("attributes");

			_jquery2.default.each(attributes, function (i, a) {
				guacaElement.attr(a.name, a.value);
			});

			guacaElement.css("display", "inline-block");
			guacaElement.addClass("guaca-editor");
			guacaElement.insertAfter(this.inputElement);

			return guacaElement;
		}
	}, {
		key: 'setUpControlHandlerService',
		value: function setUpControlHandlerService() {
			var controlHandlerService = new ControlHandlerService();
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;

			try {
				for (var _iterator2 = DefaultHandlerConfig[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var handler = _step2.value;

					var HandlerClass = handler.handlerClass;
					var handlerInstance = new HandlerClass(handler.handlerProperty);
					handlerInstance.registerMarkdownEditor(this);
					controlHandlerService.registerHandler(handler.alias, handlerInstance);
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}

			return controlHandlerService;
		}
	}, {
		key: 'subscribeToEvents',
		value: function subscribeToEvents() {
			var self = this;
			var selectStartEventName = "selectstart";
			var selectStartAvailable = typeof this.guacaEditorElement[0].onselectstart !== "undefined";
			if (!selectStartAvailable) selectStartEventName = "mousedown";

			this.guacaEditorElement.on(selectStartEventName, function () {
				self.onSelectStart();
				(0, _jquery2.default)(document).one('mouseup', function () {
					setTimeout(function () {
						self.onSelectText();
					}, 1);
				});
				return true;
			});

			this.guacaEditorElement.on('blur', function (event) {
				// neccessary in order to get next focus
				// no cross-browser solution available
				setTimeout(function () {
					self.onEditorBlur();
				}, 1);
			});

			this.guacaEditorElement.on('input', function (event) {
				var newHtml = event.target.innerHTML;
				var newMarkdown = (0, _toMarkdown2.default)(newHtml);
				self.inputElement.val(newMarkdown);
				if (self.onMarkdownUpdate) self.onMarkdownUpdate();
			});

			this.inputElement.on('input', function (event) {
				self.updateGuacaElementFromInputElement();
			});
		}
	}, {
		key: 'updateGuacaElementFromInputElement',
		value: function updateGuacaElementFromInputElement() {
			this.guacaEditorElement[0].innerHTML = (0, _marked2.default)(this.inputElement[0].value);
		}
	}, {
		key: 'onEditorBlur',
		value: function onEditorBlur() {
			var currentEle = (0, _jquery2.default)(document.activeElement);
			if (currentEle.hasClass("guaca-control")) return true;else this.airPopUp.hide();
		}
	}, {
		key: 'onSelectText',
		value: function onSelectText() {
			this.currentSelection = window.getSelection();
			if (!this.currentSelection.type && this.currentSelection.anchorOffset === this.currentSelection.focusOffset) return;
			if (this.currentSelection.type === "Caret") return;
			var rect = this.currentSelection.getRangeAt(0).getBoundingClientRect();
			this.airPopUp.setPosition(rect);
		}
	}, {
		key: 'onSelectStart',
		value: function onSelectStart() {
			this.airPopUp.hide();
		}
	}]);

	return GuacaMarkdownEditor;
}();

exports.default = GuacaMarkdownEditor;
//# sourceMappingURL=GuacaMarkdownEditor.js.map