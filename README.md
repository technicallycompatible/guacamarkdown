### Guacamarkdown ###

* Inline editor that outputs markdown, using contentEditable and to-markdown.js.
* v0.3.0

### Usage ###


```
#!bash

$ bower install guacamarkdown
```

Import ES6 module from bower_components/guacamarkdown

```
#!javascript

import GuacaMarkdownEditor from '../bower_components/guacamarkdown/dist/GuacaMarkdownEditor';
```
Check out ./example/main.js for an example, and check out ./gulpfile.js for an example of how to build.

### Dev Usage ###

* Clone repo
* Install npm packages:
```
#!sh
$ npm install

```
* Install bower: 
```
#!sh
$ npm install bower -g

```
* Install gulp:
```
#!sh
$ npm install gulp -g

```
* Install bower packages: 
```
#!sh
$ bower install

```
* To build/watch/serve the example 
```
#!sh
$ gulp

```