import $ from 'jquery';
import toMarkdown from 'to-markdown';
import marked from 'marked';

class GuacaAirPopUp {

	constructor(editorElement, controlHandlerService, settings) {
		this.settings = settings || this.getDefaultSettings();
		this.controlHandlerService = controlHandlerService;
		this.airPopUp = $('<div></div>');
		this.airPopUp.addClass("guaca-airpopup");
		this.airPopUp.insertAfter(editorElement);
		this.subscribeToEvents();
		this.currentControls = [];
		this.addControls(this.settings.controls, this.airPopUp);
		this.$window = $(window);
		this.hasFocus = false;
	}

	// API Start

	setPosition(rect) {
		var y = this.getComputedYPosition(rect.top, rect.bottom),
			x = this.getComputedXPosition(rect.left);

		this.updateControlStates();
		this.airPopUp.css("top", y);
		this.airPopUp.css("left", x);
		this.airPopUp.css("display", "block");
	}

	hide() {
		this.airPopUp.css("display", "none");
	}

	getHeight() {
		return this.airPopUp.innerHeight();
	}

	getWidth() {
		return this.airPopUp.innerWidth();
	}

	updateControlStates() {
		this.currentControls.forEach((control) => {
			var enabledState = false;
			if(control.handler)
				enabledState = control.handler.isEnabledOnSelection();
			if(enabledState) {
				control.elem.addClass(this.settings.controlActiveClass);
			} else {
				control.elem.removeClass(this.settings.controlActiveClass);
			}
		});
	}

	// API End

	subscribeToEvents() {
		var self = this;
		self.airPopUp.on("focusin", () => {
			self.hasFocus = true;
			self.airPopUp.one('focusout', () => {
				self.hasFocus = false;
			});
		});
	}

	getComputedYPosition(top, bottom) {
		var newYOffset = this.getHeight() + this.settings["y-spacing"];
		var y;
		if (top < newYOffset) y = bottom + this.settings["y-spacing"];
		else y = top - newYOffset;
		return y + this.$window.scrollTop();
	}

	getComputedXPosition(left) {
		var x = left;
		if (left + this.getWidth() + this.settings["x-spacing"] > this.$window.innerWidth())
			x = this.$window.innerWidth() - this.getWidth() - this.settings["x-spacing"];
		return x + this.$window.scrollLeft();
	}

	addControls(controls, element) {
		var self = this;
		for(var control of controls) {
			if(typeof control == "object" && control.length) {
				var container =  $('<span class="container"></span>');
				this.addControls(control, container);
				element.append(container);
			}
			var controlElem = this.createControl(control);
			element.append(controlElem);
			var controlHandler = this.controlHandlerService.getHandler(control);
			this.currentControls.push({ name: control, elem: controlElem, handler: controlHandler });
			if(controlHandler) {
				controlElem.on("click", function() {
					var controlAlias = $(this).attr("control-alias");
					var controlHandler = self.controlHandlerService.getHandler(controlAlias);
					controlHandler.execute();
					self.updateControlStates();
				});
			}
		}
	}

	createControl(className) {
		var control = $('<button></button>');
		control.addClass("guaca-control");
		control.addClass(className);
		control.attr("control-alias", className);
		return control;
	}

	getDefaultControls() {
		return [
			'bold',
			'italic',
			'divider',
			[
				'small-heading',
				'medium-heading',
				'large-heading'
			],
			'paragraph',
			'ul',
			'ol'
		];
	}

	getDefaultSettings() {
		return {
			"y-spacing": 8,
			"x-spacing": 20,
			"controls": this.getDefaultControls(),
			"controlActiveClass": "control-active"
		}
	}
}

class BaseHandler {
	constructor() {
		this.markdownEditor = null;
	}
	
	registerMarkdownEditor(markdownEditor) {
		this.markdownEditor = markdownEditor;
	}
	
	getCurrentSelection() {
		if(this.markdownEditor) {
			return this.markdownEditor.getCurrentSelection();
		}
		throw "registerMarkdownEditor must be called before calling getCurrentSelection.";
	}
	
	execute() {
		throw "Must override the execute method in extending classes";
	}
	
	isEnabledOnSelection() {
		throw "Must override the isEnabledOnSelection method in extending classes";
	}
}

class ControlHandlerService {

	constructor() {
		this.registeredHandlers = [];
	}

	registerHandler(alias, handler) {
		var isControllerInstance = handler instanceof BaseHandler;
		if(!isControllerInstance) throw "ControlHandlerService.registerHandler can only register BaseHandler extensions"
		var existingHandlerReg = this.registeredHandlers.find(h => h.handler === handler);
		if(existingHandlerReg) {
			existingHandlerReg.aliases.push(alias);
			return;
		}

		this.registeredHandlers.push({
			aliases: [
				alias
			],
			handler: handler
		});
	}

	getHandler(alias) {
		var handlerRegistration =
			this.registeredHandlers.find(h => h.aliases.some(a => a === alias));
		if(handlerRegistration)	return handlerRegistration.handler;
		else return null;
	}
}

class DocumentCommandHandler extends BaseHandler {

	constructor(commandProperty) {
		super();
		this.commandName = null;
		this.commandProperty = commandProperty;
	}

	execute() {
		if (document.execCommand && this.commandName) {
			document.execCommand(this.commandName, false, this.commandProperty);
		}
		else {
			throw "Browsers without document.execCommand are not supported in DocumentCommandHandler";
		}
	}

	isEnabledOnSelection() {
		if (document.queryCommandState && this.commandName) {
			var commandIsApplied = false;
			commandIsApplied = document.queryCommandState(this.commandName);
			return commandIsApplied;
		}
		else {
			throw "Browsers without document.queryCommandState are not supported in DocumentCommandHandler";
		}
	}
}

class BoldMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "bold";
	}
}

class ItalicMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "italic";
	}
}

class UnorderedListMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "insertUnorderedList";
	}
}

class OrderedListMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "insertOrderedList";
	}
}

class FormatBlockMarkupHandler extends DocumentCommandHandler {

	constructor(commandProperty) {
		super(commandProperty);
		this.commandName = "formatBlock";
	}
}

var DefaultHandlerConfig = [
	{
		alias: "bold",
		handlerClass: BoldMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "italic",
		handlerClass: ItalicMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "ul",
		handlerClass: UnorderedListMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "ol",
		handlerClass: OrderedListMarkupHandler,
		handlerProperty: null
	},
	{
		alias: "large-heading",
		handlerClass: FormatBlockMarkupHandler,
		handlerProperty: "H1"
	},
	{
		alias: "medium-heading",
		handlerClass: FormatBlockMarkupHandler,
		handlerProperty: "H2"
	},
	{
		alias: "small-heading",
		handlerClass: FormatBlockMarkupHandler,
		handlerProperty: "H3"
	}
]

class GuacaMarkdownEditor {

	constructor(inputElement) {
		var self = this;
		self.inputElement = inputElement;
		self.guacaEditorElement = this.setUpGuacaMarkdownElement();
		self.updateGuacaElementFromInputElement();
		self.controlHandlerService = this.setUpControlHandlerService();
		self.airPopUp = new GuacaAirPopUp(self.guacaEditorElement, self.controlHandlerService);
		self.currentSelection = null;

		inputElement.css("display", "none");
		self.subscribeToEvents();
	}

	// API Start

	getCurrentSelection() {
		return this.currentSelection;
	}

	onMarkdownUpdate() {}

	updateGuacaElement() {
		this.updateGuacaElementFromInputElement();
	}

	// API End

	setUpGuacaMarkdownElement() {
		var guacaElement = $('<div contentEditable="true" class="guaca-editor" ></div>');
		var attributes = this.inputElement.prop("attributes");

		$.each(attributes, (i, a) => {
			guacaElement.attr(a.name, a.value);
		});

		guacaElement.css("display", "inline-block");
		guacaElement.addClass("guaca-editor");
		guacaElement.insertAfter(this.inputElement);

		return guacaElement;
	}

	setUpControlHandlerService() {
		var controlHandlerService = new ControlHandlerService();
		for(var handler of DefaultHandlerConfig)
		{
			var HandlerClass = handler.handlerClass;
			var handlerInstance = new HandlerClass(handler.handlerProperty);
			handlerInstance.registerMarkdownEditor(this);
			controlHandlerService.registerHandler(handler.alias, handlerInstance);
		}

		return controlHandlerService;
	}

	subscribeToEvents() {
		var self = this;
		var selectStartEventName = "selectstart";
		var selectStartAvailable = typeof this.guacaEditorElement[0].onselectstart !== "undefined";
		if(!selectStartAvailable) selectStartEventName = "mousedown";

		this.guacaEditorElement.on(selectStartEventName, () => {
			self.onSelectStart();
			$(document).one('mouseup', () => {
				setTimeout(function(){
					self.onSelectText();
				},1);
			});
			return true;
		});

		this.guacaEditorElement.on('blur', (event) => {
			// neccessary in order to get next focus
			// no cross-browser solution available
			setTimeout(function(){
				self.onEditorBlur();
			},1);
		});

		this.guacaEditorElement.on('input', (event) => {
			var newHtml = event.target.innerHTML;
			var newMarkdown = toMarkdown(newHtml);
			self.inputElement.val(newMarkdown);
			if(self.onMarkdownUpdate)
			self.onMarkdownUpdate();
		});

		this.inputElement.on('input', (event) => {
			self.updateGuacaElementFromInputElement();
		});
	}

	updateGuacaElementFromInputElement() {
		this.guacaEditorElement[0].innerHTML = marked(this.inputElement[0].value);
	}

	onEditorBlur() {
		var currentEle = $(document.activeElement);
		if (currentEle.hasClass("guaca-control"))
			return true;
		else
			this.airPopUp.hide();
	}

	onSelectText() {
		this.currentSelection = window.getSelection();
		if(!this.currentSelection.type && this.currentSelection.anchorOffset === this.currentSelection.focusOffset)
			return;
		if(this.currentSelection.type === "Caret")
			return;
		var rect = this.currentSelection.getRangeAt(0).getBoundingClientRect();
		this.airPopUp.setPosition(rect);
	}

	onSelectStart() {
		this.airPopUp.hide();
	}
}

export default GuacaMarkdownEditor;
//# sourceMappingURL=GuacaMarkdownEditor.js.map
